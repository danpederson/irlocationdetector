import pyglet
import matplotlib
matplotlib.use('TkAgg')
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import socket
import threading
import sys
import os

#Un-comment this if using OS-X.
os.system('defaults write org.python.python ApplePersistenceIgnoreState NO')

averageWindowLength = 0.2
averageWindowSize = int(5000/8/5*averageWindowLength)

WindowSize = 5000
SampleRate = 1000.0
VoltsPerBit = 2.5/256

lastDataPoint = 0

#Define global variables
Fs = 1000
FlexWindowSize = 0.25
data = []
displayData = [-2 for i in range(WindowSize)]
flexing = False

gridPosition = [0,0]

# This reads from a socket.
def data_listener():
  global data
  UDP_PORT = 9000
  sock = socket.socket(socket.AF_INET, # Internet
                      socket.SOCK_DGRAM) # UDP
  sock.bind((UDP_IP, UDP_PORT))
  while True:
    newdata, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    dataList = []
    tempData = 0
    for i in range(len(newdata)):
      if i%2 == 0:
        tempData = newdata[i]
      else:
        dataList.append(((tempData)<<8)|newdata[i])

    data.extend(list(dataList))

    #data.extend(list(newdata))

#Handle command line arguments to get IP address
if (len(sys.argv) == 2):
    try:
        UDP_IP = sys.argv[1]
        socket.inet_aton(UDP_IP)
    except:
        sys.exit('Invalid IP address, Try again')
else:
    sys.exit('EMG_Acquire <Target IP Address>')

#Connect the UDP_Port
UDP_PORT = 9000
sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

print('Connected to ', str(UDP_IP))
print("Listening for incoming messages...")
print('Close Window to exit')

#Start a new thread to listen for data over UDP
thread = threading.Thread(target=data_listener)
thread.daemon = True
thread.start()

#Load and place image resources
pyglet.resource.path = ['./resources']
pyglet.resource.reindex()
Rat_image = pyglet.resource.image("Rat.png")
Grid_image = pyglet.resource.image("Grid.png")

# ForeArm_image.anchor_x = 7
# ForeArm_image.anchor_y = ForeArm_image.height-150
# Bicep_image.anchor_x = Bicep_image.width/2
# Bicep_image.anchor_y = Bicep_image.height/2

#Define the moving ForeArm class
class Rat(pyglet.sprite.Sprite):
  def __init__(self, *args, **kwargs):
    super(Rat,self).__init__(img=Rat_image,*args, **kwargs)	
    # self.rotate_speed = 100.0
    # self.rotation_upper_limit = -10
    # self.rotation_lower_limit = -100
    # self.rotation = self.rotation_upper_limit
    self.key_handler = pyglet.window.key.KeyStateHandler()

  def update(self, gridPosition):

    #update position of the rat...
    position = getAbsolutePosition(gridPosition)
    self.x = position[0]
    self.y = position[1]
    # if flexing:
    #   if not ((self.rotation-self.rotate_speed*dt) <=  self.rotation_lower_limit):
    #     self.rotation -= self.rotate_speed*dt
    #   else:
    #     self.rotation = self.rotation_lower_limit
    # else:
    #   if not((self.rotation+self.rotate_speed*dt) >= self.rotation_upper_limit):
    #     self.rotation += self.rotate_speed*dt
    #   else:
    #     self.rotation = self.rotation_upper_limit


#Setup the main window

main_window = pyglet.window.Window(700,700)

main_batch = pyglet.graphics.Batch()
background = pyglet.graphics.OrderedGroup(0)
foreground = pyglet.graphics.OrderedGroup(1)
grid = pyglet.sprite.Sprite(img=Grid_image,x=0,y=0,batch=main_batch,group=background)
rat = Rat(x=510, y=115,batch=main_batch,group=foreground)
pyglet.gl.glClearColor(1, 1, 1, 1)

sensorTextList = []

sensorTextList.append(pyglet.text.Label(text='N : ', x=325, y=600, batch=main_batch,group=foreground,color=[0,0,0,255]))

sensorTextList.append(pyglet.text.Label(text='E : ', x=575, y=350, batch=main_batch,group=foreground,color=[0,0,0,255]))

sensorTextList.append(pyglet.text.Label(text='S : ', x=325, y=100, batch=main_batch,group=foreground,color=[0,0,0,255]))

sensorTextList.append(pyglet.text.Label(text='W : ', x=75, y=350, batch=main_batch,group=foreground,color=[0,0,0,255]))


x = 30
y = 30
dx = 10
dy = 10

# sensorTextList.append(pyglet.text.Label(text='S4 : ', x=450, y=450, batch=main_batch,group=foreground,color=[0,0,0,255]))

# sensorTextList.append(pyglet.text.Label(text='S5 : ', x=450, y=150, batch=main_batch,group=foreground,color=[0,0,0,255]))

# sensorTextList.append(pyglet.text.Label(text='S6 : ', x=125, y=150, batch=main_batch,group=foreground,color=[0,0,0,255]))

# sensorTextList.append(pyglet.text.Label(text='S7 : ', x=125, y=450, batch=main_batch,group=foreground,color=[0,0,0,255]))

def getAbsolutePosition(gridPosition):
  return [gridPosition[0]*50+1,gridPosition[1]*50]

def update(dt):
  global displayData, data, flexing, lastDataPoint, gridPosition

  newData = list(data)
  data = []
  newDisplay = list(displayData[len(newData):len(displayData)] + newData)
  displayData = list(newDisplay)

  #Put your flex algorithm code here!
  #If flexing is detected, set the 'flexing' variable to True.
  #Otherwise, set it to False.

  sensorData = [[] for i in range(8)]

  for dataPoint in displayData:
    address = dataPoint>>10
    if (address >= 0):
      sensorData[address].append(dataPoint&0x3FF)

  #print(sensorData)
  ws = averageWindowSize

  for i in range(8):
    if (i == 3):
      if (len(sensorData[i])>0):
        #sensorTextList[0].text = 'N' + ' : ' + str(int(sum(sensorData[i])/len(sensorData[i])))
        sensorTextList[0].text = 'N' + ' : ' + str(int(sum(sensorData[i][-ws:])/len(sensorData[i][-ws])))

    elif (i == 2):
      if (len(sensorData[i])>0):
        sensorTextList[1].text = 'E' + ' : ' + str(int(sum(sensorData[i][-ws:])/len(sensorData[i][-ws])))
     
    elif (i == 0):
      if (len(sensorData[i])>0):
        sensorTextList[2].text = 'S' + ' : ' + str(int(sum(sensorData[i][-ws:])/len(sensorData[i][-ws])))
    elif (i == 5):
      if (len(sensorData[i])>0):
        sensorTextList[3].text = 'W' + ' : ' + str(int(sum(sensorData[i][-ws:])/len(sensorData[i][-ws])))
    

    #sensorTextList[i].text = 'S' + str(i) + ' : ' + str(int(sum(sensorData[i])/len(sensorData[i])))
  #gridPosition = [(gridPosition[0]+1)%14,(gridPosition[1]+1)%14]
  gridPosition = [4,7]
  rat.update(gridPosition)

@main_window.event
def on_draw():
    main_window.clear()
    main_batch.draw()

pyglet.clock.schedule_interval(update, 1/120.0)
pyglet.app.run()